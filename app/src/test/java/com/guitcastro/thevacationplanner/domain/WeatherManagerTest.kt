package com.guitcastro.thevacationplanner.domain

import com.guitcastro.thevacationplanner.ImmediateSchedulersRule
import com.guitcastro.thevacationplanner.networking.WeatherService
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Single
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is
import org.junit.Before

import org.junit.Test
import org.junit.Rule



class WeatherManagerTest {

    @JvmField
    @Rule
    var schedulers = ImmediateSchedulersRule()

    private lateinit var subject: WeatherManager
    private val api: WeatherService = mock()
    private var cityWeathers: MutableList<CityWeather> = mutableListOf()
    private val temperature = Temperature(1,1, "C")
    private val city = City("a","a", "C", "MG", "Brazil")
    private val weathers = listOf(Weather("a", "a"), Weather("b", "b"), Weather("c", "c"))

    @Before
    fun setUp() {
        subject = WeatherManagerImpl(api)
        cityWeathers.add(CityWeather("1", "2018-01-01", temperature, "a"))
        cityWeathers.add(CityWeather("2", "2018-01-02", temperature, "a"))
        cityWeathers.add(CityWeather("3", "2018-01-03", temperature, "b"))

        cityWeathers.add(CityWeather("5", "2018-01-05", temperature, "c"))
        cityWeathers.add(CityWeather("6", "2018-01-06", temperature, "c"))

        given(api.weather(any(), any())).willReturn(Single.just(cityWeathers))
    }

    @Test
    fun groupWithSuccess() {
        val list = subject.weather(city, weathers, 2).blockingGet()
        // two groups
        assertThat(list.size, Is.`is`(2))

        // 1, 2, 3
        assertThat(list[0].size, Is.`is`(3))
        // 5, 6
        assertThat(list[1].size, Is.`is`(2))
    }

    @Test
    fun groupRemoveWeathersNotSpecified() {
        val list = subject.weather(city, listOf(weathers[0],weathers[2]), 2).blockingGet()
        // two groups
        assertThat(list.size, Is.`is`(2))

        // 1, 2
        assertThat(list[0].size, Is.`is`(2))
        // 5, 6
        assertThat(list[1].size, Is.`is`(2))

    }

}