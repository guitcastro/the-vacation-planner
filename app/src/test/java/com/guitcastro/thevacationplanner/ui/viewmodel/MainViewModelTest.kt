package com.guitcastro.thevacationplanner.ui.viewmodel

import com.guitcastro.thevacationplanner.domain.City
import com.guitcastro.thevacationplanner.domain.Weather
import com.guitcastro.thevacationplanner.domain.WeatherManager
import com.nhaarman.mockito_kotlin.*
import io.reactivex.subjects.BehaviorSubject
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is.`is`
import org.hamcrest.core.IsNull
import org.junit.Before
import org.junit.Test

class MainViewModelTest {

    private lateinit var subject: MainViewModel
    private lateinit var manager: WeatherManager
    private lateinit var presenter: MainViewPresenter
    private lateinit var citiesSubject: BehaviorSubject<List<City>>
    private lateinit var weathersSubject: BehaviorSubject<List<Weather>>
    private val expectedCity = City("1", "test", "test", "test", "test")
    private val expectedWeather = Weather("1", "test")

    @Before
    fun setUp() {
        citiesSubject = BehaviorSubject.create()
        weathersSubject = BehaviorSubject.create()

        manager =  mock()
        presenter = mock()

        given(presenter.getString(any())).willReturn("Error!")

        given(manager.cities()).willReturn(citiesSubject.firstOrError())
        given(manager.weather()).willReturn(weathersSubject.firstOrError())
    }

    @Test
    fun testOnSubscribeIsLoadingIsTrueAndWhenCompleteIsLoadingIsFalse() {
        subject = MainViewModel(manager, presenter, state)

        assertThat(subject.isLoading, `is`(true))
        assertThat(subject.hasError, `is`(false))

        citiesSubject.onNext(ArrayList())

        // still have to wait for the weather

        assertThat(subject.isLoading, `is`(true))
        assertThat(subject.hasError, `is`(false))

        weathersSubject.onNext(ArrayList())

        assertThat(subject.isLoading, `is`(false))
        assertThat(subject.hasError, `is`(false))

    }

    @Test
    fun testOnErrorHasErrorIsSetToTrueAndIsLoadingToFalse() {
        subject = MainViewModel(manager, presenter, state)

        assertThat(subject.isLoading, `is`(true))
        assertThat(subject.hasError, `is`(false))

        citiesSubject.onError(mock())

        assertThat(subject.isLoading, `is`(false))
        assertThat(subject.hasError, `is`(true))
    }

    @Test
    fun testOnSuccessArraysAreFilled() {
        subject = MainViewModel(manager, presenter, state)

        weathersSubject.onNext(listOf(expectedWeather))
        citiesSubject.onNext(listOf(expectedCity))

        assertThat(subject.cities.size, `is`(1))
        assertThat(subject.weathers.size, `is`(1))
    }

    @Test
    fun testInvalidNumberOfDays() {
        subject = MainViewModel(manager, presenter, state)
        assertThat(subject.numberOfDaysErrorMessage, `is`(IsNull.nullValue()))

        subject.didClickSubmitButton()

        assertThat(subject.numberOfDaysErrorMessage, `is`(IsNull.notNullValue()))

        subject.numberOfDays = "15"

        subject.didClickSubmitButton()
        assertThat(subject.numberOfDaysErrorMessage, `is`(IsNull.nullValue()))
    }

    @Test
    fun testInvalidCitySelected() {

        subject = MainViewModel(manager, presenter, state)
        assertThat(subject.cityErrorMessage, `is`(IsNull.nullValue()))

        weathersSubject.onNext(listOf(expectedWeather))
        citiesSubject.onNext(listOf(expectedCity))

        subject.didClickSubmitButton()
        assertThat(subject.cityErrorMessage, `is`(IsNull.notNullValue()))

        subject.selectedCityPosition = 1
        subject.didClickSubmitButton()

        assertThat(subject.cityErrorMessage, `is`(IsNull.nullValue()))
    }
}