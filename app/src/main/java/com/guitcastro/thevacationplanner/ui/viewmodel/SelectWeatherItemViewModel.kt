package com.guitcastro.thevacationplanner.ui.viewmodel

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.guitcastro.thevacationplanner.BR
import com.guitcastro.thevacationplanner.binding.DataBinding
import com.guitcastro.thevacationplanner.domain.Weather

class SelectWeatherItemViewModel(val weather: Weather) : BaseObservable() {

    val resId = "ic_weather_${weather.id}"
    @get:Bindable
    var isChecked: Boolean by DataBinding(BR.checked, this, false)

    fun didTapItem() {
        isChecked = !isChecked
    }
}