package com.guitcastro.thevacationplanner.ui.viewmodel

import com.guitcastro.thevacationplanner.domain.SelectWeatherViewData
import com.guitcastro.thevacationplanner.utils.StringProvider

interface MainViewPresenter : StringProvider {
    fun presentSelectWeatherView(data: SelectWeatherViewData)
}