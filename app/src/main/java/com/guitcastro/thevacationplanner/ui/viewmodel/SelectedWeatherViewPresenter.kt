package com.guitcastro.thevacationplanner.ui.viewmodel

import com.guitcastro.thevacationplanner.domain.CityWeather

interface SelectedWeatherViewPresenter {

    fun showResult(list: List<List<CityWeather>>)

}