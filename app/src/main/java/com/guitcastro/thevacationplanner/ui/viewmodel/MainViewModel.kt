package com.guitcastro.thevacationplanner.ui.viewmodel

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.guitcastro.thevacationplanner.BR
import com.guitcastro.thevacationplanner.R
import com.guitcastro.thevacationplanner.binding.DataBinding
import com.guitcastro.thevacationplanner.domain.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.zipWith
import javax.inject.Inject

class MainViewModel @Inject constructor(private var manager: WeatherManager,
                                        private var presenter: MainViewPresenter,
                                        state: MainViewState? = null) : BaseObservable(), ViewModel {

    override val compositeDisposable = CompositeDisposable()

    @get:Bindable
    var isLoading: Boolean by DataBinding(BR.loading, this, false)
    @get:Bindable
    var hasError: Boolean by DataBinding(BR.hasError, this, false)

    @get:Bindable
    var numberOfDaysErrorMessage: String? by DataBinding(BR.numberOfDaysErrorMessage, this, null)

    @get:Bindable
    var cityErrorMessage: String? by DataBinding(BR.cityErrorMessage, this, null)

    @get:Bindable
    var cities : List<City> by DataBinding(BR.cities, this, ArrayList())

    var weathers :List<Weather> = ArrayList()

    var selectedCityPosition:Int = 0
    var numberOfDays: String? = null

    init {
        if (state != null) {
            restoreState(state)
        } else {
            fetchCitiesAndWeather()
        }
    }

    private fun restoreState(state: MainViewState) {
        this.cities = state.cities
        this.weathers = state.weathers
        this.selectedCityPosition = state.selectedCity
        this.numberOfDays = state.numberOfDays
        if (state.cities.isEmpty() || state.weathers.isEmpty()) {
            fetchCitiesAndWeather()
        }
    }

    fun fetchCitiesAndWeather() {
        this.manager.cities().zipWith(this.manager.weather(), { cities, weathers ->
            Pair(cities, weathers)
        }).doOnSubscribe { isLoading = true; hasError = false }
                .subscribeBy(
                onSuccess = {
                    isLoading = false
                    cities = it.first
                    weathers = it.second
                }, onError = {
                    isLoading = false
                    hasError = true
                }
        ).addToVMDisposables()
    }

    fun didClickSubmitButton() {
        validateNumberOfDaysInput()
        validateSelectedCity()

        if (numberOfDaysErrorMessage == null && cityErrorMessage == null) {
            presenter.presentSelectWeatherView(SelectWeatherViewData(this.numberOfDays!!,
                    this.cities.get(this.selectedCityPosition - 1),
                    weathers))
        }
    }

    private fun validateNumberOfDaysInput() {
        if (numberOfDays.isNullOrBlank()) {
            this.numberOfDaysErrorMessage = presenter.getString(R.string.empty_field)
        } else {
            try {
                this.numberOfDays?.toInt()
                this.numberOfDaysErrorMessage = null
            } catch (e: NumberFormatException) {
                this.numberOfDaysErrorMessage = presenter.getString(R.string.invalid_format)
            }
        }
    }

    private fun validateSelectedCity() {
        if (selectedCityPosition == 0) {
            this.cityErrorMessage = presenter.getString(R.string.empty_field)
        } else {
            this.cityErrorMessage = null
        }
    }

    fun saveState() : MainViewState = MainViewState(selectedCityPosition, numberOfDays, weathers, cities)

}