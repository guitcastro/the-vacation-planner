package com.guitcastro.thevacationplanner.ui.viewmodel

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

interface ViewModel {

    val compositeDisposable: CompositeDisposable

    fun Disposable.addToVMDisposables() {
        compositeDisposable.add(this)
    }

    fun onDestroy() {
        this.compositeDisposable.dispose()
    }
}
