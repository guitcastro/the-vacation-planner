package com.guitcastro.thevacationplanner.ui.viewmodel

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.guitcastro.thevacationplanner.BR
import com.guitcastro.thevacationplanner.R
import com.guitcastro.thevacationplanner.binding.DataBinding
import com.guitcastro.thevacationplanner.domain.SelectWeatherViewData
import com.guitcastro.thevacationplanner.domain.WeatherManager
import io.reactivex.rxkotlin.subscribeBy
import me.tatarka.bindingcollectionadapter2.ItemBinding

class SelectWeatherViewModel(private val data: SelectWeatherViewData,
                             private val manager: WeatherManager,
                             private val presenter: SelectedWeatherViewPresenter) : BaseObservable() {

    val itemBinding: ItemBinding<SelectWeatherItemViewModel> = ItemBinding.of(BR.viewModel, R.layout.weather_item_view)
    val items = data.weathers.map {
        SelectWeatherItemViewModel(it)
    }

    @get:Bindable
    var isLoading: Boolean by DataBinding(BR.loading, this, false)

    fun didTapSubmit() {
        val selectedWeathers = items.filter { it.isChecked }.map { it.weather }
        manager.weather(data.city, selectedWeathers, data.numberOfDays.toInt())
                .doOnSubscribe { isLoading = true }
                .doAfterTerminate { isLoading = false  }
                .subscribeBy(onSuccess = {
                    presenter.showResult(it)
                }, onError = {
                    it.printStackTrace()
                })
    }
}