package com.guitcastro.thevacationplanner

import android.app.Activity
import android.app.Application
import com.guitcastro.thevacationplanner.dagger.DaggerInjectorComponent
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject


class VacationPlannerApp : Application(), HasActivityInjector {

    @Inject
    lateinit var androidInjector : DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)

        DaggerInjectorComponent.builder()
                .build()
                .inject(this)
    }


    override fun activityInjector(): AndroidInjector<Activity> = androidInjector


}
