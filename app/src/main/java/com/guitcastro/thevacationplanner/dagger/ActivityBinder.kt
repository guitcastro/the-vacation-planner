package com.guitcastro.thevacationplanner.dagger


import com.guitcastro.thevacationplanner.MainView
import com.guitcastro.thevacationplanner.SelectWeatherView

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ActivityBinder {

    @ContributesAndroidInjector
    internal abstract fun mainView(): MainView
    @ContributesAndroidInjector
    internal abstract fun selectWeatherView(): SelectWeatherView
}