package com.guitcastro.thevacationplanner.dagger

import com.guitcastro.thevacationplanner.networking.WeatherService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
class NetModule {

    private companion object {
        val API_URL = "https://mysterious-lowlands-46432.herokuapp.com"
    }

    @Provides
    fun weatherApiProvider() = Retrofit.Builder()
            .baseUrl(API_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(WeatherService::class.java)


}