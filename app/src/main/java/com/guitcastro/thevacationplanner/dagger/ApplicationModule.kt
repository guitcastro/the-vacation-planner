package com.guitcastro.thevacationplanner.dagger

import com.guitcastro.thevacationplanner.domain.WeatherManager
import com.guitcastro.thevacationplanner.domain.WeatherManagerImpl
import com.guitcastro.thevacationplanner.networking.WeatherService
import dagger.Module
import dagger.Provides


@Module
class ApplicationModule {
    @Provides
    fun providesWeatherManager(service: WeatherService) : WeatherManager = WeatherManagerImpl(service)

}