package com.guitcastro.thevacationplanner.dagger

import com.guitcastro.thevacationplanner.VacationPlannerApp
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class,
        ActivityBinder::class,
        ApplicationModule::class,
        NetModule::class))
interface InjectorComponent {
    fun inject(application: VacationPlannerApp)
}

