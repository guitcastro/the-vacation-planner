package com.guitcastro.thevacationplanner.domain

import org.threeten.bp.LocalDate
import java.util.*

data class CityWeather(val woeid:String,
                       val date:String,
                       val temperature: Temperature,
                       val weather: String) {

    val localDate: LocalDate
        get() {
            val year = Calendar.getInstance().get(Calendar.YEAR)
            val monthAndYear = this.date.substring(this.date.length - 5)
            return LocalDate.parse("$year-$monthAndYear")
        }

}
