package com.guitcastro.thevacationplanner.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Weather (val id:String, val name:String) : Parcelable
