package com.guitcastro.thevacationplanner.domain

import com.guitcastro.thevacationplanner.networking.WeatherService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class WeatherManagerImpl @Inject constructor(private val service: WeatherService) : WeatherManager {

    override fun weather() : Single<List<Weather>> = service.weather()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .cache()

    override fun cities(): Single<List<City>> = service.cities()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .cache()

    private fun filterAndGroupList(list: List<CityWeather>) : List<List<CityWeather>> {

        if (list.isEmpty()) {
            return listOf()
        }

        val result: MutableList<List<CityWeather>> = mutableListOf()
        var temp:MutableList<CityWeather> = mutableListOf()
        val sortedList = list.sortedBy { it.localDate.dayOfYear }
        temp.add(sortedList.get(0))

        for (i in 0 until sortedList.size - 1) {
            if (sortedList[i + 1].localDate.dayOfYear == sortedList[i].localDate.dayOfYear.plus(1)) {
                temp.add(sortedList[i + 1])
            } else {
                result.add(temp)
                temp = mutableListOf()
                temp.add(sortedList[i + 1])
            }
        }
        result.add(temp)
        return result
    }

    override fun weather(city: City, weathers: List<Weather>, numberOfDays: Int): Single<List<List<CityWeather>>> {
        val year = Calendar.getInstance().get(Calendar.YEAR)
        val weathersName = weathers.map { it.name }
        return service.weather(city.woeid, year)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { it.filter { weathersName.contains(it.weather) } }
                .map { filterAndGroupList(it) }
                .map { it.filter { it.size >= numberOfDays } }
    }



}