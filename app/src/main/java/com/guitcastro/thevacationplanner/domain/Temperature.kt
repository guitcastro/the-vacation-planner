package com.guitcastro.thevacationplanner.domain

data class Temperature(val max: Int, val min: Int, val unit: String)
