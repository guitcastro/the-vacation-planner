package com.guitcastro.thevacationplanner.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SelectWeatherViewData(val numberOfDays:String, val city: City, val weathers: List<Weather>) : Parcelable
