package com.guitcastro.thevacationplanner.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class City(val woeid:String,
                val district:String,
                val province:String,
                val state_acronym:String,
                val country:String) :Parcelable {

    override fun toString(): String {
        return district
    }

}