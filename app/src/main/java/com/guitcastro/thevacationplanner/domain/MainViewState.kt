package com.guitcastro.thevacationplanner.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MainViewState(val selectedCity: Int,
                         val numberOfDays: String?,
                         val weathers: List<Weather>,
                         val cities: List<City>) : Parcelable