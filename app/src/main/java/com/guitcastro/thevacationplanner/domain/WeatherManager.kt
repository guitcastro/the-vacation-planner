package com.guitcastro.thevacationplanner.domain

import io.reactivex.Single

interface WeatherManager {
    fun cities(): Single<List<City>>
    fun weather(): Single<List<Weather>>
    fun weather(city: City, weathers: List<Weather>, numberOfDays: Int): Single<List<List<CityWeather>>>
}