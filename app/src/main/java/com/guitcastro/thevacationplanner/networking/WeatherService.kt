package com.guitcastro.thevacationplanner.networking

import com.guitcastro.thevacationplanner.domain.City
import com.guitcastro.thevacationplanner.domain.CityWeather
import com.guitcastro.thevacationplanner.domain.Weather
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path


interface WeatherService {

    @GET("/cities/")
    fun cities() : Single<List<City>>

    @GET("/weather/")
    fun weather() : Single<List<Weather>>

    @GET("/cities/{cityId}/year/{year}")
    fun weather(@Path("cityId") cityId:String, @Path("year") year:Int) : Single<List<CityWeather>>


}