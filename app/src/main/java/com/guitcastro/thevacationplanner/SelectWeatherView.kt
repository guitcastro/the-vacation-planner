package com.guitcastro.thevacationplanner

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.guitcastro.thevacationplanner.databinding.SelectWeatherViewBinding
import com.guitcastro.thevacationplanner.domain.CityWeather
import com.guitcastro.thevacationplanner.domain.SelectWeatherViewData
import com.guitcastro.thevacationplanner.domain.WeatherManager
import com.guitcastro.thevacationplanner.ui.viewmodel.SelectWeatherViewModel
import com.guitcastro.thevacationplanner.ui.viewmodel.SelectedWeatherViewPresenter
import dagger.android.AndroidInjection
import org.threeten.bp.DateTimeUtils
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class SelectWeatherView : AppCompatActivity(), SelectedWeatherViewPresenter {


    companion object {
        const val DATA_EXTRA = "data"
    }

    @Inject
    lateinit var manager: WeatherManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)

        val binding:SelectWeatherViewBinding = DataBindingUtil.setContentView(this, R.layout.select_weather_view)

        val data: SelectWeatherViewData = intent.getParcelableExtra(DATA_EXTRA)

        binding.viewModel = SelectWeatherViewModel(data, manager, this)
    }

    override fun showResult(list: List<List<CityWeather>>) {
        val text = StringBuilder("")
        val dateFormatter = SimpleDateFormat("dd/MM", Locale.getDefault())

        for (days in list) {
            text.append(dateFormatter.format(days.first().localDate.toDate()))
                    .append(" - ")
                    .append(dateFormatter.format(days.last().localDate.toDate()))
                    .append("\n")
        }

        if (list.isEmpty()) {
            text.append("No dates were available with the criteria you specify")
        }

        AlertDialog.Builder(this).setTitle("Available dates")
                .setMessage(text.toString())
                .setCancelable(true)
                .setPositiveButton("Ok", { _, _ -> }).show()
    }

    fun LocalDate.toDate() : Date = DateTimeUtils.toDate(this.atStartOfDay(ZoneId.systemDefault()).toInstant())

}