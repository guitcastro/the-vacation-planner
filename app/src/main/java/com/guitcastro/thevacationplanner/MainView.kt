package com.guitcastro.thevacationplanner

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.guitcastro.thevacationplanner.databinding.MainViewBinding
import com.guitcastro.thevacationplanner.domain.MainViewState
import com.guitcastro.thevacationplanner.domain.SelectWeatherViewData
import com.guitcastro.thevacationplanner.domain.WeatherManager
import com.guitcastro.thevacationplanner.ui.viewmodel.MainViewModel
import com.guitcastro.thevacationplanner.ui.viewmodel.MainViewPresenter
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainView : AppCompatActivity(), MainViewPresenter {

    companion object {
        const val MAIN_VIEW_STATE_KEY = "how_many_days"
    }

    @Inject
    lateinit var service: WeatherManager

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)

        val state: MainViewState? = savedInstanceState?.getParcelable(MAIN_VIEW_STATE_KEY)

        if (state != null) {
            this.viewModel = MainViewModel(service, this, state)
        } else {
            this.viewModel = MainViewModel(service, this)
        }

        val binding: MainViewBinding =  DataBindingUtil.setContentView(this, R.layout.main_view)
        binding.viewModel = viewModel
    }

    override fun onDestroy() {
        super.onDestroy()
        this.viewModel.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putParcelable(MAIN_VIEW_STATE_KEY, viewModel.saveState())
        super.onSaveInstanceState(outState)
    }

    override fun presentSelectWeatherView(data: SelectWeatherViewData) {
        val intent = Intent(this, SelectWeatherView::class.java)
        intent.putExtra(SelectWeatherView.DATA_EXTRA, data)
        startActivity(intent)
    }


}
