package com.guitcastro.thevacationplanner.utils

import android.support.annotation.StringRes

interface StringProvider {

    fun getString(@StringRes resId: Int) : String

}