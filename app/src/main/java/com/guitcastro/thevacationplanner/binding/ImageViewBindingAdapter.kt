package com.guitcastro.thevacationplanner.binding

import android.databinding.BindingAdapter
import android.widget.ImageView

object ImageViewBindingAdapter {

    @BindingAdapter("resId")
    @JvmStatic
    fun setDrawable(view: ImageView, resId: String) {
        val id = view.context.resources.getIdentifier(resId, "drawable", view.context.packageName)
        view.setImageResource(id)
    }
}