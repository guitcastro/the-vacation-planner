package com.guitcastro.thevacationplanner.binding


import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.databinding.InverseBindingListener
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import fr.ganfra.materialspinner.MaterialSpinner

object MaterialSpinnerBindingAdapter {

    @BindingAdapter("adapterItems")
    @JvmStatic
    fun setItems(view: MaterialSpinner, items: List<*>) {
        val adapter = ArrayAdapter(view.context, android.R.layout.simple_spinner_item, items)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        view.adapter = adapter
    }


    @BindingAdapter("error")
    @JvmStatic
    fun setError(view: MaterialSpinner, error: String?) {
        view.error = error
    }

    @BindingAdapter("selectedPosition", "selectedItemEvent", requireAll = false)
    @JvmStatic
    fun setSelectedItem(view: MaterialSpinner,
                        selectedItemPosition: Int = 0,
                        inverseBindingListener: InverseBindingListener) {
        view.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                inverseBindingListener.onChange()
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                inverseBindingListener.onChange()
            }

        }
        view.setSelection(selectedItemPosition)
    }

    @JvmStatic
    @InverseBindingAdapter(attribute = "selectedPosition", event = "selectedItemEvent")
    fun captureSelectedPosition(view: MaterialSpinner): Int {
        return view.selectedItemPosition
    }

}