package com.guitcastro.thevacationplanner.binding;

import android.databinding.BaseObservable
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class DataBinding<T>(private val fieldId: Int, private val baseObservable: BaseObservable, private var value: T) : ReadWriteProperty<Any?, T> {

    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = value
        baseObservable.notifyPropertyChanged(fieldId)
    }
}