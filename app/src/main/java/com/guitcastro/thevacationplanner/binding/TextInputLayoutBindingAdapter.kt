package com.guitcastro.thevacationplanner.binding

import android.R
import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.databinding.InverseBindingListener
import android.support.design.widget.TextInputLayout
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import fr.ganfra.materialspinner.MaterialSpinner

object TextInputLayoutBindingAdapter {

    @BindingAdapter("errorMessage")
    @JvmStatic
    fun setError(view: TextInputLayout, error: String?) {
        if (error == null) {
            view.isErrorEnabled = false
        } else {
            view.isCounterEnabled = true
            view.error = error
        }
    }



}