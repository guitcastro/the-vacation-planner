package com.guitcastro.thevacationplanner.binding


import android.databinding.BindingAdapter
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton
import br.com.simplepass.loading_button_lib.interfaces.AnimatedButton
import br.com.simplepass.loading_button_lib.interfaces.OnAnimationEndListener

object AnimatedViewBindingAdapter {

    @JvmStatic
    @BindingAdapter(value = *arrayOf("loading", "onAnimationEndListener"), requireAll = false)
    fun animatedCircularButtonBinding(animatedView: CircularProgressButton,
                                      animate: Boolean,
                                      onAnimationEndListener: OnAnimationEndListener?) {
        animatedViewBinding(animatedView, animate, onAnimationEndListener)
    }

    private fun animatedViewBinding(animatedView: AnimatedButton,
                                    animate: Boolean,
                                    onAnimationEndListener: OnAnimationEndListener?) {
        if (animate) {
            animatedView.startAnimation()
        } else {
            if (onAnimationEndListener != null) {
                animatedView.revertAnimation(onAnimationEndListener)
            } else {
                animatedView.revertAnimation()
            }
        }
    }


    private fun animatedViewDispose(animatedView: AnimatedButton, dispose: Boolean) {
        if (dispose) {
            animatedView.dispose()
        }
    }

    @JvmStatic
    @BindingAdapter("dispose")
    fun animatedCircularButtonDispose(animatedView: CircularProgressButton, dispose: Boolean) {
        animatedViewDispose(animatedView, dispose)
    }

}