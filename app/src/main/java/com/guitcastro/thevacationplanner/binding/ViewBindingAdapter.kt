package com.guitcastro.thevacationplanner.binding

import android.databinding.BindingAdapter
import android.view.View

object ViewBindingAdapter {

    @BindingAdapter("android:visibility")
    @JvmStatic
    fun setVisibility(view: View, shouldDisplay: Boolean) {
        view.visibility = if (shouldDisplay) View.VISIBLE else View.GONE
    }
}