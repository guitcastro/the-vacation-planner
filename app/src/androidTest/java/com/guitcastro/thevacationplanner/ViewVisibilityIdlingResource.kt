package com.guitcastro.thevacationplanner

import android.app.Activity
import android.os.Handler
import android.support.annotation.IdRes
import android.support.test.espresso.IdlingResource
import android.view.View
import java.lang.ref.WeakReference

/**
 * [IdlingResource] which monitors a [View] for a given visibility state. The resource is considered idle when the
 * View has the desired state.

 * @author vaughandroid@gmail.com
 */
class ViewVisibilityIdlingResource
/**
 * @param view the View to monitor
 * *
 * @param visibility One of [View.VISIBLE], [View.INVISIBLE], or [View.GONE].
 */
(view: View, private val visibility: Int) : IdlingResource {

    /** Hold weak reference to the View, so we don't leak memory even if the resource isn't unregistered.  */
    private val view: WeakReference<View>
    private val name: String

    private var resourceCallback: IdlingResource.ResourceCallback? = null

    /**
     * @param activity which owns the View
     * *
     * @param viewId ID of the View to monitor
     * *
     * @param visibility One of [View.VISIBLE], [View.INVISIBLE], or [View.GONE].
     */
    constructor(activity: Activity, @IdRes viewId: Int, visibility: Int) : this(activity.findViewById<View>(viewId), visibility) {}

    init {
        this.view = WeakReference(view)
        name = "View Visibility for view " + view.id + "(@" + System.identityHashCode(this.view) + ")"
    }

    override fun getName(): String {
        return name
    }

    override fun isIdleNow(): Boolean {
        val view = view.get()
        val isIdle = view == null || view.visibility == visibility
        if (isIdle) {
            if (resourceCallback != null) {
                resourceCallback!!.onTransitionToIdle()
            }
        } else {
            /* Force a re-check of the idle state in a little while.
             * If isIdleNow() returns false, Espresso only polls it every few seconds which can slow down our tests.
             * Ideally we would watch for the visibility state changing, but AFAIK we can't detect when a View's
             * visibility changes to GONE.
             */
            Handler().postDelayed({ isIdleNow }, IDLE_POLL_DELAY_MILLIS.toLong())
        }

        return isIdle
    }

    override fun registerIdleTransitionCallback(resourceCallback: IdlingResource.ResourceCallback) {
        this.resourceCallback = resourceCallback
    }

    companion object {
        private val IDLE_POLL_DELAY_MILLIS = 100
    }
}