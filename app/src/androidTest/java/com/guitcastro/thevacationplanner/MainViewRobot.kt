package com.guitcastro.thevacationplanner

import android.support.design.widget.TextInputLayout
import android.support.test.espresso.Espresso.onData
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.matcher.ViewMatchers.withId
import 	android.support.test.espresso.assertion.ViewAssertions.*
import android.view.View
import com.guitcastro.thevacationplanner.domain.City
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher


class MainViewRobot {

    fun fillNumberOfDays(days: String) {
        onView(withId(R.id.editText)).perform(typeText(days))
    }

    fun selectedCityAtPosition(position: Int) {
        onView(withId(R.id.spinner)).perform(click())
        onData(allOf(`is`(instanceOf(City::class.java)))).atPosition(position).perform(click())
    }

    fun clickSubmit() {
        onView(withId(R.id.button)).perform(click())
    }

    fun numberOfDayHasError(error: String) {
        onView(withId(R.id.text_input_layout))
                .check(matches(this.hasTextInputLayoutHintText(error)))
    }


    fun hasTextInputLayoutHintText (expectedErrorText:String) : Matcher<View> {
        return object: TypeSafeMatcher<View>() {
            override fun matchesSafely(item: View?): Boolean {
                if (!(item is TextInputLayout)) {
                    return false;
                }

                val error = item.error ?: return false;


                return expectedErrorText == (error.toString())
            }

            override fun describeTo(description: Description?) {
            }
    }
}
}