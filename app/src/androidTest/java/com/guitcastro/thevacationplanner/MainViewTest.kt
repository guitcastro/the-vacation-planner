package com.guitcastro.thevacationplanner

import android.content.Intent
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.espresso.intent.matcher.IntentMatchers.*
import android.support.test.espresso.intent.Intents.*
import android.view.View
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MainViewTest {
    @Rule
    @JvmField
    var activityTestRule = IntentsTestRule(MainView::class.java, true, false)

    private lateinit var idlingResource: ViewVisibilityIdlingResource

    @Before
    fun setUp() {
        activityTestRule.launchActivity(Intent())
        this.idlingResource = ViewVisibilityIdlingResource(
                activityTestRule.activity.findViewById(R.id.loadingLayout), View.GONE)
        IdlingRegistry.getInstance().register(this.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(this.idlingResource)
    }


    @Test
    fun testNavigateToSelectWeatherView() {
        robot {
            fillNumberOfDays("15")
            selectedCityAtPosition(0)
            clickSubmit()
        }

        intended(hasComponent(SelectWeatherView::class.java.getName()))
    }

    @Test
    fun testShowErrorWhenSubmitEmptyText() {
        robot {
            clickSubmit()
            numberOfDayHasError(activityTestRule.activity.getString(R.string.empty_field))
        }

    }


    fun robot (func : MainViewRobot.() -> Unit) =
            MainViewRobot().apply { func() }

}